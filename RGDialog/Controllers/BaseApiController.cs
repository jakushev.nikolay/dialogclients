﻿using Microsoft.AspNetCore.Mvc;

namespace RGDialog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
    }
}
