﻿using Microsoft.AspNetCore.Mvc;
using RGDialog.Service;
using System;
using System.Collections.Generic;

namespace RGDialog.Controllers
{
    public class DialogController : BaseApiController
    {
        private readonly IServiceRepository _service;

        public DialogController(IServiceRepository service)
        {
            this._service = service;
        }

        [HttpGet("id")]
        public ActionResult<Guid> GuidGetDialog([FromQuery]IEnumerable<Guid> clients)
        {
            return _service.GetDialogs(clients);
        }
    }
}
