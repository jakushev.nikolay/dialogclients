﻿using System;
using System.ComponentModel.DataAnnotations;

namespace RGDialog.Entities
{
    public class RGDialogClient
    {
        [Key]
        public Guid IDUnique { get; set; }
        public Guid IDRGDialog { get; set; }
        public Guid IDClient { get; set; }
        public DateTime? DateEvent { get; set; }
    }
}
