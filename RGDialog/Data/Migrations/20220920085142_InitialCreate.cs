﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace RGDialog.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    IDUnique = table.Column<Guid>(type: "TEXT", nullable: false),
                    IDRGDialog = table.Column<Guid>(type: "TEXT", nullable: false),
                    IDClient = table.Column<Guid>(type: "TEXT", nullable: false),
                    DateEvent = table.Column<DateTime>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.IDUnique);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clients");
        }
    }
}
