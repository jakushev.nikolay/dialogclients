﻿using Microsoft.EntityFrameworkCore;
using RGDialog.Entities;

namespace RGDialog.Data
{
    public class StoreContext : DbContext
    {
        public StoreContext(DbContextOptions options) : base(options) { }

        public DbSet<RGDialogClient> Clients { get; set; }


    }
}
