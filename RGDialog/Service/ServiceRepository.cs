﻿using RGDialog.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RGDialog.Service
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly IRepositoryDao _repository;

        public ServiceRepository(IRepositoryDao repository)
        {
            this._repository = repository;
        }

        public Guid GetDialogs(IEnumerable<Guid> clients)
        {
            var dialogs = _repository.GetDialogs().Result
                .GroupBy(x => x.IDRGDialog)
                .OrderBy(x => x.Count());

            foreach (var dialog in dialogs)
            {
                var countClient = dialog.Select(x => x.IDClient).Intersect(clients).Count();
                if (clients.Count() != countClient) { continue; }
                return dialog.Key;
            }
            return Guid.Empty;
        }
    }
}
