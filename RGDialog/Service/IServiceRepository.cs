﻿using System;
using System.Collections.Generic;

namespace RGDialog.Service
{
    public interface IServiceRepository
    {
        public Guid GetDialogs(IEnumerable<Guid> clients);
    }
}
