﻿using Microsoft.EntityFrameworkCore;
using RGDialog.Data;
using RGDialog.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RGDialog.Repository
{
    public class RepositoryDao : IRepositoryDao
    {
        private readonly StoreContext _context;

        public RepositoryDao(StoreContext context)
        {
            this._context = context;
        }

        public async Task<IEnumerable<RGDialogClient>> GetDialogs()
        {
            return await _context.Clients.ToListAsync();
        }
    }
}
