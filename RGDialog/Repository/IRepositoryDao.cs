﻿using RGDialog.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RGDialog.Repository
{
    public interface IRepositoryDao
    {
        public Task<IEnumerable<RGDialogClient>> GetDialogs();
    }
}
